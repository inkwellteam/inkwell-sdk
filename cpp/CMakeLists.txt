cmake_minimum_required (VERSION 2.6)
project (Inkwell_SDK)
SET (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)
SET (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/lib)
SET (CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/lib)

INCLUDE(TestBigEndian)
TEST_BIG_ENDIAN(BIGENDIAN)
IF(${BIGENDIAN})
     ADD_DEFINITIONS(-DBIGENDIAN)
ENDIF(${BIGENDIAN})

include_directories("${PROJECT_SOURCE_DIR}/include")

SET (CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake_modules)

set(Boost_USE_STATIC_LIBS        ON)
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)
find_package(Boost 1.46.0 COMPONENTS filesystem system)
if(Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIRS})
endif()

find_package(GLM)
if(GLM_FOUND)
  include_directories(${GLM_INCLUDE_DIRS})
endif()

add_executable(ink_mdl_compiler src/ink_mdl_compiler.cpp src/ink_mdl_compiler_cli.cpp)
if(Boost_FOUND)
  target_link_libraries(ink_mdl_compiler ${Boost_LIBRARIES})
endif()
