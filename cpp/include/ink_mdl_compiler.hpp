#define BOOST_FILESYSTEM_NO_DEPRECATED

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace ink_geom_compiler {
  struct Bone {
    unsigned char parent;
    glm::vec3 pos;
    std::string name;
  };
  struct Point {
    glm::vec3 pos;
    unsigned char bone;
    unsigned char weight;
    Point() {}
  };
  struct Vertex {
    uint p;
    glm::vec3 n;
    glm::vec2 t;
  };
  struct SemiVertex {
    uint p;
    uint n;
    uint t;
    bool operator==(SemiVertex sv) {
      return sv.p == p && sv.n == n && sv.t == t;
    }
  };
  struct Face {
    uint verts[3];
  };
  class Model {
  private:
    std::vector<Point> points;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
    std::vector<SemiVertex> vertices;
    std::vector<Face> faces;
  public:
    Model() {}
    ~Model() {}
    std::string loadOBJ(const boost::filesystem::path& filename, bool *ok);
    std::string loadOBJ(std::istream *objfile, bool *ok);
    bool loadOBJline(const std::string line, std::string *error);
    void writeIGEOM(std::ostream *igeomfile);

    //Returns default SemiVertex on failure to parse. Be warned!
    static SemiVertex getVert(const std::string vert) {
      char *next;
      SemiVertex out;
      out.p = uint(strtoul(vert.c_str(), &next, 0)-1);
      if(*next == '\0') {
        out.n = 0;
        out.t = 0;
        return out;
      }
      next++;
      if(*next == '/') {
        out.t = 0;
      } else {
        out.t = uint(strtoul(next, &next, 0)-1);
      }
      if(*next == '\0') {
        out.n = 0;
      } else {
        out.n = uint(strtoul(next+1, NULL, 0)-1);
      }
      return out;
    }
  };

  int doCompile(char *input_file, char *output_file, bool quiet, bool wtsout);
}
