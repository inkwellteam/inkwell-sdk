#include <ink_mdl_compiler.hpp>
#include <sstream>
#include <boost/algorithm/string.hpp>

#define INK_GEOM_VERSION 0

namespace ink_geom_compiler {
  //Returns true on success. If and only if there is a problem,
  //describes it into error. This doesn't always mean failure.
  bool Model::loadOBJline(const std::string line, std::string *error) {
    bool he = (error != NULL);
    std::vector<std::string> parts;
    boost::split(parts, line, boost::is_any_of(" \t"), boost::token_compress_on);
    if(parts.size() < 1) return true;
    if(parts[0] == "v") {
      if(parts.size() < 4) {
        if(he) *error = "Error: Not enough information for one point!";
        return false;
      }
      if(parts.size() > 4) {
        if(he) *error = "Warning: Extra information on point?";
      }
      glm::vec3 pos(strtod(parts[1].c_str(), NULL), strtod(parts[2].c_str(), NULL), strtod(parts[3].c_str(), NULL));
      Point p;
      p.pos = pos; p.bone = 1; p.weight = 0;
      points.push_back(p);
    } else if(parts[0] == "vt") {
      if(parts.size() < 3) {
        if(he) *error = "Error: Not enough information for one texture coordinate!";
        return false;
      }
      if(parts.size() > 3) {
        if(he) *error = "Warning: Extra information on texture coordinate?";
      }
      glm::vec2 vt(strtod(parts[1].c_str(), NULL), strtod(parts[2].c_str(), NULL));
      texcoords.push_back(vt);
    } else if(parts[0] == "vn") {
      if(parts.size() < 4) {
        if(he) *error = "Error: Not enough information for one normal!";
        return false;
      }
      if(parts.size() > 4) {
        if(he) *error = "Warning: Extra information on normal?";
      }
      glm::vec3 vn(strtod(parts[1].c_str(), NULL), strtod(parts[2].c_str(), NULL), strtod(parts[3].c_str(), NULL));
      normals.push_back(vn);
    } else if(parts[0] == "f") {
      if(parts.size() < 4) {
        if(he) *error =
          "Warning: Polygon has too few verts! Skipping.";
        return true;
      }
      if(parts.size() > 4) {
        if(he) *error =
          "Warning: This isn't a triangle, "
          "so it isn't really supported.";
        return true;
      }
      uint svs[3];
      for(uint i = 0; i < 3; i++) {
        SemiVertex sv = getVert(parts[i+1]);
        bool go = true;
        for(uint j = 0; j < vertices.size(); j++) {
          if(vertices[j] == sv) {
            go = false;
            svs[i] = j;
            break;
          }
        }
        if(go) {
          svs[i] = vertices.size();
          vertices.push_back(sv);
        }
      }
      Face f;
      f.verts[0] = svs[0]; f.verts[1] = svs[1]; f.verts[2] = svs[2];
      faces.push_back(f);
    }
    return true;
  }
  std::string Model::loadOBJ(std::istream *objfile, bool *ok) {
    std::stringstream ss;
    std::string line;
    for(uint i = 1; objfile->good(); i++) {
      std::getline(*objfile, line);
      std::string err = "No error";
      bool success = loadOBJline(line, &err);
      if(err != "No error") {
        ss << "Line " << i << ": " << err << std::endl;
      }
      if(!success) {
        *ok = false;
        return ss.str();
      }
    }
    if(vertices.size() > 0) {
      if(points.size() <= 0) {
        points.push_back(Point());
      }
      if(normals.size() <= 0) {
        normals.push_back(glm::vec3(0));
      }
      if(texcoords.size() <= 0) {
        texcoords.push_back(glm::vec2(0));
      }
    }
    return ss.str();
  }
  std::string Model::loadOBJ(const boost::filesystem::path& filename, bool *ok) {
    std::stringstream ss;
    if(!boost::filesystem::exists(filename)) {
      ss << filename << " does not exist." << std::endl;
      *ok = false;
      return ss.str();
    }
    if(!boost::filesystem::is_regular_file(filename)) {
      ss << filename << " is not a regular file." << std::endl;
      *ok = false;
      return ss.str();
    }
    boost::filesystem::ifstream objfile(filename);
    if(!objfile.is_open()) {
      ss << "Unable to open file" << std::endl;
      *ok = false;
      return ss.str();
    }
    return loadOBJ(&objfile, ok);
  }
  template <class t>
  inline void tostream(std::ostream *str, const t& info) {
    #ifdef BIGENDIAN
    str->write((char*)(&info), sizeof(info));
    #endif
    #ifndef BIGENDIAN
    char *p = (char*)(&info);
    for(int i = sizeof(info)-1; i >= 0; i--) {
      str->put(*(p+i));
    }
    #endif
  }
  inline void tostream(std::ostream *str, std::string info) {
    str->write(info.c_str(), info.size());
  }
  template <class t>
  inline void tostream(std::ostream *str, const t& info, size_t *i) {
    tostream(str, info);
    *i += sizeof(info);
  }
  inline void tostream(std::ostream *str, std::string info, size_t *i) {
    tostream(str, info);
    *i += info.size();
  }
  void Model::writeIGEOM(std::ostream *igeomfile) {
    tostream(igeomfile, int(0xCEF55425));
    tostream(igeomfile, short(INK_GEOM_VERSION));
    tostream(igeomfile, char(2));
    //Writing bones
    size_t s = 0;
    std::ostringstream ss(std::ios::out | std::ios::binary);
    std::string bonename = "root_bone";
    tostream(&ss, glm::vec3(0.0f), &s);
    tostream(&ss, char(bonename.length()), &s);
    tostream(&ss, bonename, &s);
    tostream(&ss, '\0', &s);
    bonename = "main_bone";
    tostream(&ss, char(0), &s);
    tostream(&ss, glm::vec3(0.0f), &s);
    tostream(&ss, char(bonename.length()), &s);
    tostream(&ss, bonename, &s);
    tostream(&ss, '\0', &s);
    tostream(igeomfile, short(s));
    igeomfile->write(ss.str().c_str(), s);
    //Writing points
    tostream(igeomfile, int(points.size()));
    for(int i = 0; i < points.size(); i++) {
      tostream(igeomfile, points[i].pos.x);
      tostream(igeomfile, points[i].pos.y);
      tostream(igeomfile, points[i].pos.z);
      tostream(igeomfile, points[i].bone);
      tostream(igeomfile, points[i].weight);
    }
    //Writing vertices
    tostream(igeomfile, int(vertices.size()));
    for(int i = 0; i < vertices.size(); i++) {
      tostream(igeomfile, vertices[i].p);
      tostream(igeomfile, normals[vertices[i].n].x);
      tostream(igeomfile, normals[vertices[i].n].y);
      tostream(igeomfile, normals[vertices[i].n].z);
      tostream(igeomfile, texcoords[vertices[i].t].x);
      tostream(igeomfile, texcoords[vertices[i].t].y);
    }
    //Writing faces
    tostream(igeomfile, int(faces.size()));
    for(int i = 0; i < faces.size(); i++) {
      tostream(igeomfile, faces[i].verts[0]);
      tostream(igeomfile, faces[i].verts[1]);
      tostream(igeomfile, faces[i].verts[2]);
    }
  }

  int doCompile(char *input_file, char *output_file, bool quiet, bool wtsout) {
    boost::filesystem::path infile(input_file);
    boost::filesystem::path outfile;
    if(output_file == NULL) {
      outfile = input_file;
      outfile.replace_extension(".igeom");
    } else {
      outfile = output_file;
    }
    bool ok = true;
    Model mdl;
    std::string error = mdl.loadOBJ(infile, &ok);
    if(!quiet) std::cerr << error;
    if(!ok) {
      return 1;
    }
    std::ostream *out = NULL;
    if(wtsout) {
      out = &std::cout;
    } else {
      out = new boost::filesystem::ofstream(outfile, std::ios::out | std::ios::binary | std::ios::trunc);
    }
    mdl.writeIGEOM(out);
    if(!wtsout) {
      delete out;
    }
  }
}
