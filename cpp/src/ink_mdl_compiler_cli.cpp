#include <argp.h>
#include <ink_mdl_compiler.hpp>

#define ARGP_VERSION_STRING "ink_mdl_compiler (Inkwell SDK) 1.0"
#define ARGP_BUG_ADDRESS "<bugs@inkwell.xenotoad.net>"
#define ARGP_DOCS "The Inkwell Geometry Compiler\nCompiles OBJ files into Inkwell geometry files (.igeom)."
#define ARGP_ARGS "inputfile"

const char *argp_program_version = ARGP_VERSION_STRING;
const char *argp_program_bug_address = ARGP_BUG_ADDRESS;

static struct argp_option options[] = {
  {"quiet",    'q', 0,      0,  "Don't display messages" },
  {"silent",   's', 0,      OPTION_ALIAS },
  {"output",   'o', "FILE", 0,  "Write model to FILE" },
  {"stdout",   'd', 0,      0,  "Write model to standard output"},
  { 0 }
};

struct argumentss {
  char *output_file;
  char *input_file;
  bool quiet;
  bool wtsout;
  argumentss() {output_file = NULL; input_file = NULL; quiet = false; wtsout = false;}
};
argumentss arguments;
static error_t parse_opt(int key, char* arg, struct argp_state *state) {
  argumentss *arguments = (argumentss*)(state->input);
  switch(key) {
    case 'q': case 's':
      arguments->quiet = true;
      break;
    case 'o':
      arguments->output_file = arg;
      break;
    case 'd':
      arguments->wtsout = true;
    case ARGP_KEY_ARG:
      if(state->arg_num >= 1) {
        argp_usage(state);
      }
      arguments->input_file = arg;
      break; 
    case ARGP_KEY_END:
      if(state->arg_num < 1) {
        argp_usage(state);
      }
      break;
    default:
      return ARGP_ERR_UNKNOWN;
  }
  return 0;
}
static argp argp = {options, parse_opt, ARGP_ARGS, ARGP_DOCS};

int main(int argc, char *argv[]) {
  argp_parse(&argp, argc, argv, 0,0, &arguments);
  return ink_geom_compiler::doCompile(arguments.input_file, arguments.output_file, arguments.quiet, arguments.wtsout);
}
