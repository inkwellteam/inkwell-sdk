def tris2obj(tris):
  verts = [None]
  for i in tris:
    for a in i.verts:
      if(not a.id in verts):
        verts.append(a.id)
        print("v " + str(a.point.pos.x) + " " + str(a.point.pos.y) + " " + str(a.point.pos.z))
  for a in tris:
    print("f " + str(verts.index(a.a.id)) + " " + str(verts.index(a.b.id)) + " " + str(verts.index(a.c.id)))

def p3d2obj(p3d):
  for a in p3d.verts:
    print("v " + str(a.point.pos.x) + " " + str(a.point.pos.y) + " " + str(a.point.pos.z))
  print "f ",
  for i in range(len(p3d.verts)):
    print i+1,
