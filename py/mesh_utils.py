import math

def abtequals(x, y):
  return abs(x-y) < 0.001
class vec3:
  def __init__(self, x=0, y=None, z=None):
    if(y==None):
      y = x
    if(z==None):
      z = x
    self.x = float(x)
    self.y = float(y)
    self.z = float(z)
  def xy(self):
    return vec2(self.x, self.y)
  def xz(self):
    return vec2(self.x, self.z)
  def yz(self):
    return vec2(self.y, self.z)
  def z_up(self):
    return vec3(self.x, self.z, self.y)
  def get_length(self):
    return math.sqrt(self.x*self.x+self.y*self.y+self.z*self.z)
  def sub(self, other):
    return vec3(self.x - other.x, self.y - other.y, self.z - other.z)
  def normalize(self):
    l = self.get_length()
    return vec3(self.x/l, self.y/l, self.z/l)
  def equiv(self, other):
    return self.x==other.x and self.y==other.y and self.z==other.z
  def as_tuple(self):
    return (self.x, self.y, self.z)
  def __repr__(self):
    return "vec3(%.1f, %.1f, %.1f)" % (self.x, self.y, self.z)
class vec2:
  def __init__(self, x=0, y=None):
    if(y==None):
      y = x
    self.x = float(x)
    self.y = float(y)
  def cross(self, other):
    return self.x * other.y - other.x * self.y
  def sub(self, other):
    return vec2(self.x - other.x, self.y - other.y)
  def flipX(self):
    self.x = -self.x
    return self
  def as_tuple(self):
    return (self.x, self.y)
  def __repr__(self):
    return "vec2(%.1f, %.1f)" % (self.x, self.y)
class Bone:
  def __init__(self, name, pos, parent, id=0):
    self.name = name # string
    self.pos = pos # vec3
    self.parent = parent # Bone
    self.id = id
  def __repr__(self):
    return "Bone named " + self.name
class Point:
  def __init__(self, pos, bone, weight=0, id=0):
    self.pos = pos # vec3
    self.bone = bone # Bone
    self.weight = weight # int 0-255
    self.id = id
  def __repr__(self):
    return "Point at %r" % self.pos
class Vertex:
  def __init__(self, point, normal, tex, id=0):
    self.point = point # Point
    self.normal = normal # vec3
    self.tex = tex # vec2
    self.id = id
  def __repr__(self):
    return "Vertex at %r" % self.point.pos
class Triangle:
  def __init__(self, *verts):
    self.verts = verts
    self.a = verts[0] # Vertex
    self.b = verts[1]
    self.c = verts[2]

class Poly3D:
  def __init__(self, *verts):
    self.verts = list(verts)
  def to2D(self):
    sv = [a.point.pos for a in self.verts]
    if(all(abtequals(a.z, sv[0].z) for a in sv)):
      q = [a.xy() for a in sv]
      return Poly2D(*q).makeCW()
    elif(all(abtequals(a.y, sv[0].y) for a in sv)):
      q = [a.xz() for a in sv]
      return Poly2D(*q).makeCW()
    else:
      q = [a.yz() for a in sv]
      return Poly2D(*q).makeCW()
  def copy(self):
    return Poly3D(*self.verts)
  def __repr__(self):
    return "Poly3D with %d verts" % len(verts)
class Poly2D:
  def __init__(self, *verts):
    self.verts = list(verts)
  def flipX(self):
    for i in self.verts:
      i.flipX()
    return self
  def isCCW(self):
    n, s, e, w = 0, 0, 0, 0
    for i in range(1, len(self.verts)):
      a = self.verts[i]
      if(a.y > self.verts[n].y):
        n = i
      if(a.y < self.verts[s].y):
        s = i
      if(a.x > self.verts[e].x):
        e = i
      if(a.x < self.verts[w].x):
        w = i
    n, s, e, w = 0, s-n, e-n, w-n
    l = len(self.verts)
    s, e, w = s%l, e%l, w%l
    return w < e
  def makeCW(self):
    if(self.isCCW()):
      self.flipX()
    return self
  def __repr__(self):
    return "Poly2D with %d verts" % len(verts)

def get_iso_vert(p3d):
  for i in range(len(p3d.verts)):
    up = (i+1)%len(p3d.verts)
    do = (i-1)%len(p3d.verts)
    d1=p3d.verts[i].point.pos.sub(p3d.verts[do].point.pos).normalize()
    d2=p3d.verts[up].point.pos.sub(p3d.verts[i].point.pos).normalize()
    if(d1.equiv(d2)):
      return i
  return -1
def is_inside(p, tri):
  ab = tri.verts[1].sub(tri.verts[0])
  bc = tri.verts[2].sub(tri.verts[1])
  ca = tri.verts[0].sub(tri.verts[2])
  ap = p.sub(tri.verts[0])
  bp = p.sub(tri.verts[1])
  cp = p.sub(tri.verts[2])
  cra = ab.cross(ap)
  crb = bc.cross(bp)
  crc = ca.cross(cp)
  return ((abs(cra) == cra) == (abs(crb) == crb) \
    and (abs(cra) == cra) == (abs(crc) == crc))
def get_ear(i, p2d):
  i = i % len(p2d.verts)
  up = (i+1) % len(p2d.verts)
  do = (i-1) % len(p2d.verts)
  d1 = p2d.verts[i].sub(p2d.verts[do])
  d2 = p2d.verts[up].sub(p2d.verts[i])
  cross = d1.cross(d2)
  if(cross > 0):
    return None
  tri = Poly2D(p2d.verts[i], p2d.verts[up], p2d.verts[do])
  for j in range(len(p2d.verts)):
    if(j == i or j == up or j == do):
      continue
    if(is_inside(p2d.verts[j], tri)):
      return None
  return tri
def trilate_poly(poly):
  if(len(poly.verts) < 3):
    return []
  elif(len(poly.verts) == 3):
    return [Triangle(*poly.verts)]
  else:
    p3d = poly.copy()
    p2d = poly.to2D()
    out = []
    while(len(p3d.verts) > 3):
      ii = 0
      nt = None
      for i in range(len(p3d.verts)):
        ear = get_ear(i, p2d)
        if(ear != None):
          ii = i
          nt = ear
          break
      del p2d.verts[ii]
      out.append(Triangle(p3d.verts[(ii-1) % len(p3d.verts)], \
        p3d.verts[ii], p3d.verts[(ii+1) % len(p3d.verts)]))
      del p3d.verts[ii]
    out.append(Triangle(p3d.verts[0], p3d.verts[1], p3d.verts[2]))
    return out
