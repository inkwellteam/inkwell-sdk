from mesh_utils import *

def add_vert_ids(verts):
  for i in range(len(verts)):
    verts[i].id = i

# chevron
vecs = [vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 0.0), vec3(0.0, -1.0, 0.0), vec3(-1.0, 1.0, 0.0)]
points = [Point(x) for x in vecs]
verts = [Vertex(x, vec3(0.0, 0.0, 0.0), vec2(0.0, 0.0)) for x in points]
add_vert_ids(verts)
chevron = Poly3D(*verts)

# cool hexagon thing
vecs = [vec3(-1.0, 1.0, 0.0), vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 0.0), vec3(1.0, -1.0, 0.0), vec3(0.0, -2.0, 0.0), vec3(-1.0, -1.0, 0.0)]
points = [Point(x) for x in vecs]
verts = [Vertex(x, vec3(0.0, 0.0, 0.0), vec2(0.0, 0.0)) for x in points]
add_vert_ids(verts)
hexa = Poly3D(*verts)

# star
vecs = [vec3(0.0, math.cos(math.pi * 0.2 * x) * (math.cos(math.pi * x)+2) * 0.5, math.sin(math.pi * 0.2 * x) * (math.cos(math.pi * x)+2) * 0.5) for x in range(10)]
points = [Point(x) for x in vecs]
verts = [Vertex(x, vec3(0.0, 0.0, 0.0), vec2(0.0, 0.0)) for x in points]
add_vert_ids(verts)
star = Poly3D(*verts)

# bird head thing
vechs = [None, vec3(1.15861111, -0.73194444, 0.0),
vec3(0.68750000, 0.68750000, 0.0),
vec3(-1.21905556, 1.11950000, 0.0),
vec3(-0.68750000, -0.68750000, 0.0),
vec3(1.47616667, 0.25955556, 0.0),
vec3(0.0, -0.93750000, 0.0),
vec3(0.0, 0.93750000, 0.0),
vec3(-0.98905556, 0.38222222, 0.0),
vec3(0.56388889, -0.22211111, 0.0),
vec3(0.46566667, -1.09544444, 0.0),
vec3(0.37500000, 0.87500000, 0.0),
vec3(-1.29455556, 0.33055556, 0.0),
vec3(0.87500000, 0.37500000, 0.0),
vec3(-0.24166667, -0.49988889, 0.0),
vec3(-0.29500000, 0.45900000, 0.0),
vec3(-0.96566667, -0.92788889, 0.0)]
vecs = [vechs[9], vechs[5], vechs[13], vechs[2], vechs[11], vechs[7], vechs[15], vechs[3], vechs[12], vechs[8], vechs[16], vechs[4], vechs[14], vechs[6], vechs[10], vechs[1]]
points = [Point(x) for x in vecs]
verts = [Vertex(x, vec3(0.0, 0.0, 0.0), vec2(0.0, 0.0)) for x in points]
add_vert_ids(verts)
birdhead = Poly3D(*verts)
