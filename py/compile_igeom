#!/usr/bin/env python

import igeom_compilation as igc
from getopt import getopt, GetoptError
import sys
import io

usage = "Usage: compile_igeom [-qhuvs] [-i TYPE] [-o TYPE] [-b NAME] INFILE OUTFILE"
help = usage + "\nConvert between IGEOM and OBJ files." \
"\nUse a / as the infile or outfile to use standard input or output.\n" \
"\nMandatory arguments to long options are mandatory to short options, too." \
"\n  -q, --quiet\t\tsilence all command line output" \
"\n  -h, --help\t\tprint this help message" \
"\n  -u, --usage\t\tprint usage information" \
"\n  -v, --version\t\tprint version" \
"\n  -s, --strip\t\tstrip armature from model" \
"\n  -i, --intype=TYPE\tspecify input file format" \
"\n  -o, --outtype=TYPE\tspecify output file format" \
"\n  -b, --rootbone=NAME\tspecify name of root bone" \
"\n\nReport bugs to inkbugs@pitzik4.net."

version = "IGEOM compiler version 1:0:0, supports IGEOM version " + str(igc.igeom_version)

opts = None
try:
  opts = getopt(sys.argv[1:], "qi:o:huvb:s", ["quiet", "intype=", "outtype=", "help", "usage", "version", "rootbone=", "strip"])
except GetoptError as e:
  print(e.msg)
  sys.exit(1)
args = opts[1]
opts = opts[0]
for i in range(len(opts)):
  if(opts[i][0] == "--quiet"):
    opts[i] = ("-q", opts[i][1])
  elif(opts[i][0] == "--intype"):
    opts[i] = ("-i", opts[i][1])
  elif(opts[i][0] == "--outtype"):
    opts[i] = ("-o", opts[i][1])
  elif(opts[i][0] == "--help"):
    opts[i] = ("-h", opts[i][1])
  elif(opts[i][0] == "--usage"):
    opts[i] = ("-u", opts[i][1])
  elif(opts[i][0] == "--version"):
    opts[i] = ("-v", opts[i][1])
  elif(opts[i][0] == "--rootbone"):
    opts[i] = ("-r", opts[i][1])
  elif(opts[i][0] == "--strip"):
    opts[i] = ("-s", opts[i][1])
quiet = ('-q','') in opts
igc.quiet = quiet

if(('-h','') in opts):
  if not quiet:
    print(help)
  sys.exit(0)
if(('-u','') in opts):
  if not quiet:
    print(usage)
  sys.exit(0)
if(('-v','') in opts):
  if not quiet:
    print(version)
  sys.exit(0)

root_bone = "root_bone"
specroot = False
infile = None
outfile = None
intype = None
outtype = None
for a in opts:
  if(a[0] == '-i'):
    intype = a[1]
  elif(a[0] == '-o'):
    outtype = a[1]
  elif(a[0] == '-b'):
    root_bone = a[1]
    specroot = True
if(len(args) < 2):
  print("input and output files not specified")
  sys.exit(1)
infile = args[0]
outfile = args[1]
if(intype == None):
  ife = infile.split(".")
  intype = ife[len(ife)-1].upper()
else:
  intype = intype.upper()
if(outtype == None):
  ofe = outfile.split(".")
  outtype = ofe[len(ofe)-1].upper()
else:
  outtype = outtype.upper()
instream = None
outstream = None
if(infile == "/"):
  instream = sys.stdin
  infile = "standard input"
if(outfile == "/"):
  outstream = sys.stdout
  outfile = "standard output"

geom = igc.Geom(instream, intype, infile, root_bone, specroot, ('-s','') in opts)
if(outtype == "OBJ"):
  geom.write_to_OBJ(outfile, outstream)
elif(outtype == "IGEOM"):
  geom.write_to_IGEOM(outfile, outstream)
if not quiet:
  print("Conversion complete.")
