import io
from mesh_utils import *
import struct
from collada import *

quiet = False

igeom_version = 1

class EMSG:
  def __init__(self, out, tolerance=1):
    self.out = out
    self.tole = tolerance
    self.times = 0
  def pr(self, ln):
    if(self.times < self.tole and not quiet):
      self.times += 1
      print("Line " + str(ln) + ": " + out)
EMSG.neipoint = EMSG("Error: Not enough information for a point!")
EMSG.tmipoint = EMSG("Warning: Extra information for point")
EMSG.neitexcoord = EMSG("Error: Not enough information for a texcoord!")
EMSG.tmitexcoord = EMSG("Warning: Extra information for texcoord")
EMSG.neinormal = EMSG("Error: Not enough information for a normal!")
EMSG.tminormal = EMSG("Warning: Extra information for normal")
EMSG.bigon = EMSG("Warning: Discarding polygon with less than 3 points")
EMSG.tmivert = EMSG("Warning: More than 3 attributes specified on vertex")
EMSG.neivert = EMSG("Warning: Cannot generate vertex normals or texcoords")

class Geom:
  def __init__(self, fstr=None, ftype="OBJ", fname=None, root_bone="root_bone", specroot=False, strip=False):
    self.arm_ex = False
    self.bones = []
    self.points = []
    self.verts = []
    self.tris = []
    self.root_bone = root_bone
    self.specroot = specroot
    self.strip = strip
    doclose = False
    if(ftype == "COLLADA"):
      ftype = "DAE"
    if(fstr == None and fname != None):
      if(ftype == "IGEOM"):
        fstr = io.open(fname, 'rb')
        doclose = True
      elif(ftype != "DAE"):
        fstr = io.open(fname, 'rt')
        doclose = True
    elif(fname == None):
      fname = "unknown"
    
    if(ftype=="DAE"):
      self.load_from_COLLADA(None, fname)
    elif(fstr != None):
      if(ftype=="IGEOM"):
        self.load_from_IGEOM(fstr, fname)
      else:
        self.load_from_OBJ(fstr, fname)
    if(doclose):
      fstr.close()
  def load_OBJ_line_1(self, line, vts, vns, ln=0):
    s = line.split(" ")
    if(len(s) <= 0):
      return
    if(s[0] == "v"):
      if(len(s) < 4):
        EMSG.neipoint.pr(ln)
        return
      if(len(s) > 4):
        EMSG.tmipoint.pr(ln)
      self.points.append(Point(vec3(s[1], s[2], s[3]), self.bones[1], id=len(self.points)))
    elif(s[0] == "vt"):
      if(len(s) < 3):
        EMSG.neitexcoord.pr(ln)
        return
      if(len(s) > 3):
        EMSG.tmitexcoord.pr(ln)
      vts.append(vec2(float(s[1]), float(s[2])))
    elif(s[0] == "vn"):
      if(len(s) < 4):
        EMSG.neinormal.pr(ln)
        return
      if(len(s) > 4):
        EMSG.tminormal.pr(ln)
      vns.append(vec3(float(s[1]), float(s[2]), float(s[3])))
  def load_OBJ_line_2(self, line, verts, vts, vns, ln=0):
    s = line.split(" ")
    if(len(s) <= 0):
      return
    if(s[0] == "f"):
      if(len(s) < 4):
        EMSG.bigon.pr(ln)
        return
      vs = []
      for v in s[1:]:
        if(len(v) > 1):
          q = v.split("/")
          r = tuple([int(x) for x in q])
          if(len(r) > 3):
            EMSG.tmivert.pr(ln)
            r = (r[0], r[1], r[2])
          elif(len(r) < 3 or '' in r):
            EMSG.neivert.pr(ln)
            r = (r[0], r[0], r[0])
          if(r not in verts):
            verts[r] = Vertex(self.points[r[0]-1], vns[r[2]-1], vts[r[1]-1], len(verts))
          vs.append(verts[r])
      p3d = Poly3D(*vs)
      tris = trilate_poly(p3d)
      for i in tris:
        self.tris.append(i)
  def load_from_OBJ(self, fstr=None, fname="unknown"):
    if(not quiet):
      print("Loading " + fname + " as OBJ...")
    
    doclose = False
    if(fstr == None):
      fstr = io.open(fname, 'rt')
      doclose = True
    
    self.arm_ex = False
    if len(self.bones) == 0:
      self.bones.append(Bone(root_bone,vec3(0),None,0))
    if len(self.bones) < 2:
      self.bones.append(Bone("main_bone",vec3(0),self.bones[0],len(self.bones)))
    
    verts = dict()
    vts = []
    vns = []
    
    ln = 0
    lines = []
    while True:
      l = fstr.readline()
      ln = ln+1
      if(len(l) <= 0):
        break
      lines.append(l)
      self.load_OBJ_line_1(l, vts, vns, ln)
    ln = 0
    for l in lines:
      ln = ln+1
      self.load_OBJ_line_2(l, verts, vts, vns, ln)
    for i in range(len(verts)):
      self.verts.append(0)
    for i in verts:
      self.verts[verts[i].id] = verts[i]
    if(doclose):
      fstr.close()
    if not quiet:
      print("Done.")
  def write_to_OBJ(self, fname=None, fstr=None):
    if(fstr == None and fname == None):
      return
    if not quiet:
      print("Writing " + fname + " as OBJ...")
    doclose = False
    if(fstr == None):
      fstr = io.open(fname, 'wt')
      doclose = True
    vns = {}
    vts = {}
    vnl = []
    vtl = []
    fstr.write(unicode("o inkwell_object\n"))
    for i in self.points:
      fstr.write(unicode("v " + str(i.pos.x) + " " + str(i.pos.y) + " " + str(i.pos.z) + "\n"))
    for a in self.verts:
      an = a.normal.as_tuple()
      at = a.tex.as_tuple()
      if(not an in vns):
        vnl.append(an)
        vns[an] = len(vnl)
      if(not at in vts):
        vtl.append(at)
        vts[at] = len(vtl)
    for a in vnl:
      fstr.write(unicode("vn " + str(a[0]) + " " + str(a[1]) + " " + str(a[2]) + "\n"))
    for a in vtl:
      fstr.write(unicode("vt " + str(a[0]) + " " + str(a[1]) + "\n"))
    for a in self.tris:
      fstr.write(unicode("f "))
      q = 0
      for b in a.verts:
        fstr.write(unicode(str(b.point.id+1) + "/"))
        fstr.write(unicode(str(vts[b.tex.as_tuple()]) + "/"))
        fstr.write(unicode(str(vns[b.normal.as_tuple()])))
        q = q + 1
        if(q < 3):
          fstr.write(unicode(" "))
        else:
          fstr.write(unicode("\n"))
    if doclose:
      fstr.close()
    if not quiet:
      print("Done.")
  def load_from_IGEOM(self, fstr=None, fname="unknown"):
    if(not quiet):
      print("Loading " + fname + " as IGEOM...")
    
    doclose = False
    if(fstr == None):
      fstr = io.open(fname, 'rb')
      doclose = True
    
    if(fstr.read(4) != '\xCE\xF5\x54\x25'):
      if(not quiet):
        print("Error: " + fname + " is not an IGEOM file!")
      if(doclose):
        fstr.close()
      return
    vers = struct.unpack(">H", fstr.read(2))[0]
    if(vers > igeom_version):
      if(not quiet):
        print("Error: " + fname + " is IGEOM version " + vers + "; maximum allowed is " + igeom_version)
      if(doclose):
        fstr.close()
      return
    if vers > 0:
      l = struct.unpack(">B", fstr.read(1))[0]
      arm_ex = (l == 0)
    else:
      arm_ex = True
    if self.strip:
      self.arm_ex = False
    else:
      self.arm_ex = arm_ex
    if arm_ex:
      amtbones = struct.unpack(">B", fstr.read(1))[0]
      bonebytes1 = struct.unpack(">H", fstr.read(2))[0]
      bonebytes2 = 0
      for i in range(amtbones):
        parent = 0
        if(i != 0):
          parent = struct.unpack(">B", fstr.read(1))[0]
          bonebytes2 = bonebytes2 + 1
        pos = vec3(*struct.unpack(">fff", fstr.read(12)))
        l = struct.unpack(">B", fstr.read(1))[0]
        name = fstr.read(l)
        fstr.read(1)
        self.bones.append(Bone(name, pos, self.bones[parent] if i != 0 else None, i))
        bonebytes2 = bonebytes2 + 14 + l
      if(bonebytes1 != bonebytes2):
        if(not quiet):
          print("Warning: Bones in " + fname + " don't have the correct byte length!")
    else:
      if len(self.bones) == 0:
        self.bones.append(Bone(root_bone,vec3(0),None,0))
      if len(self.bones) < 2:
        self.bones.append(Bone("main_bone",vec3(0),self.bones[0],len(self.bones)))
      if not self.specroot:
        self.root_bone = fstr.read(l)
      else:
        fstr.read(l)
      
    amtpoints = struct.unpack(">I", fstr.read(4))[0]
    for i in range(amtpoints):
      pos = vec3(*struct.unpack(">fff", fstr.read(12)))
      if arm_ex:
        bone, weight = struct.unpack(">BB", fstr.read(2))
      else:
        bone, weight = 1, 0
      self.points.append(Point(pos, self.bones[bone], weight, i))
    amtverts = struct.unpack(">I", fstr.read(4))[0]
    for i in range(amtverts):
      point = struct.unpack(">I", fstr.read(4))[0]
      normal = vec3(*struct.unpack(">fff", fstr.read(12)))
      texcoord = vec2(*struct.unpack(">ff", fstr.read(8)))
      self.verts.append(Vertex(self.points[point], normal, texcoord, i))
    amttris = struct.unpack(">I", fstr.read(4))[0]
    for i in range(amttris):
      vs = struct.unpack(">III", fstr.read(12))
      vs2 = (self.verts[x] for x in vs)
      self.tris.append(Triangle(*vs2))
    
    if(doclose):
      fstr.close()
    if not quiet:
      print("Done.")
  def write_to_IGEOM(self, fname=None, fstr=None):
    if(fstr == None and fname == None):
      return
    if not quiet:
      print("Writing " + fname + " as IGEOM...")
    doclose = False
    if(fstr == None):
      fstr = io.open(fname, 'wb')
      doclose = True
    
    fstr.write('\xCE\xF5\x54\x25')
    fstr.write(struct.pack(">H", igeom_version))
    
    if self.arm_ex:
      fstr.write('\x00')
      fstr.write(struct.pack(">B", len(self.bones)))
      bstr = io.BytesIO()
      for i in range(len(self.bones)):
        b = self.bones[i]
        if(i != 0):
          bstr.write(struct.pack(">B", b.parent.id))
        bstr.write(struct.pack(">fff", b.pos.x, b.pos.y, b.pos.z))
        bstr.write(struct.pack(">B", len(b.name)))
        bstr.write(b.name)
        bstr.write('\x00')
      bstri = bstr.getvalue()
      fstr.write(struct.pack(">H", len(bstri)))
      fstr.write(bstri)
    else:
      fstr.write(struct.pack(">B", len(self.root_bone)))
      fstr.write(self.root_bone)
      fstr.write('\x00')
    
    fstr.write(struct.pack(">I", len(self.points)))
    for p in self.points:
      fstr.write(struct.pack(">fff", p.pos.x, p.pos.y, p.pos.z))
      if self.arm_ex:
        fstr.write(struct.pack(">B", p.bone.id))
        fstr.write(struct.pack(">B", p.weight))
    
    fstr.write(struct.pack(">I", len(self.verts)))
    for v in self.verts:
      fstr.write(struct.pack(">I", v.point.id))
      fstr.write(struct.pack(">fff", v.normal.x, v.normal.y, v.normal.z))
      fstr.write(struct.pack(">ff", v.tex.x, v.tex.y))
    
    fstr.write(struct.pack(">I", len(self.tris)))
    for t in self.tris:
      fstr.write(struct.pack(">III", t.a.id, t.b.id, t.c.id))
    
    if doclose:
      fstr.close()
    if not quiet:
      print("Done.")
  def load_from_COLLADA(self, fstr=None, fname="unknown"):
    if(not quiet):
      print("Loading " + fname + " as COLLADA...")
    if fstr == None:
      mesh = Collada(fname)
    else:
      mesh = Collada(fstr)
    
    if mesh.scene == None:
      geos = mesh.geometries
    else:
      geos = mesh.scene.objects('geometry')
    
    boffs = {}
    gskns = {}
    amtbones = 0
    for sk in mesh.controllers:
      if(not hasattr(sk,'joint_source')):
        continue
      boff = len(self.bones)
      boffs[sk.id] = boff
      if(sk.geometry in gskns):
        gskns[sk.geometry].append(sk)
      else:
        gskns[sk.geometry] = [sk]
      jsource = sk.sourcebyid[sk.joint_source]
      for i in xrange(len(jsource)):
        bname = jsource[i]
        m = sk.joint_matrices[bname]
        bpos = vec3(m[0][3],-m[2][3],-m[1][3])
        self.bones.append(Bone(bname,bpos,bparent,len(self.bones)))
        amtbones += 1
    self.arm_ex = amtbones > 0
    if len(self.bones) == 0:
      self.bones.append(Bone(root_bone,vec3(0),None,0))
    if len(self.bones) < 2:
      self.bones.append(Bone("main_bone",vec3(0),self.bones[0],len(self.bones)))
    
    for geo in geos:
      gskn = gskns[geo]
      gpoint = 0
      for prim in geo.primitives:
        if 'Line' in prim.__class__.__name__:
          continue
        try:
          prim = prim.triangleset()
        except:
          pass
        
        poffs = len(self.points)
        for p in prim.vertex:
          po = Point(vec3(p[0],p[2],p[1]),None,0,len(self.points))
          if(amtbones == 0):
            po.bone = self.bones[1]
          else:
            pass
          self.points.append(po)
          gpoint += 1
        
        if prim.normal == None:
          prim.generateNormals()
    
    if self.strip:
      self.arm_ex = False
    
    if not quiet:
      print("Done.")
